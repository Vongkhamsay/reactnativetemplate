import { } from "./domain";

export interface IAsynchronousAction {
	inProcess: boolean;
	error: boolean;
}

export interface ILoader<T> extends IAsynchronousAction {
	data: T;
}

export interface IAppState {
    jsonTestData: ILoader<any>
}
