import React from 'react';
import {
    Text,
    CameraRoll,
    PermissionsAndroid,
    ScrollView,
    FlatList,
    Image,
    TouchableOpacity,
    View,
    Platform,
    AppState,
} from 'react-native';
import * as JsonTestDataActions from '../../actions/JsonTestDataActions';
import { connect } from 'react-redux';



import { Container, Header, Footer, Left, Title, Body, Right, Button, Icon, FooterTab, Picker } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Actions } from 'react-native-router-flux';
import { IAppState, ILoader } from '../../appstate';

interface IAppContainerProp {
    jsonTest: ILoader<any>
}

interface IAppContainerState {
    jsonTestData: any
}

interface IActions {
    getJsonTestData: () => Promise<any>
}

interface IConnectedLoginProps extends IAppContainerProp, IActions { }


class DashboardContainer extends React.Component<IConnectedLoginProps, IAppContainerState> {

    constructor(props) {
        super(props);
        this.state = {
            jsonTestData: ''
        };
    }

    componentDidMount = () => {
        console.log("Did mount dashboard");
        console.log(this.props);
        this.props.getJsonTestData().then(res => {
            console.log(res);
            this.setState({
                jsonTestData: res.payload.data
            }, () => {
                console.log(this.state)
            });
        })
    }

    // On suspend of app
    componentWillUnmount() {
        // AppState.removeEventListener('change', this.handleAppStateChange);
    }


    render() {
        return (
            <Container>
                <Text>Hello World</Text>
            </Container>

        );

    }
}

const mapDispatchToProps = (dispatch: any) => ({
    getJsonTestData: () => dispatch(JsonTestDataActions.getUserAgency())
});

const mapStateToProps = (state: IAppState): IAppContainerProp => {
    return {
        jsonTest: state.jsonTestData
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DashboardContainer);
