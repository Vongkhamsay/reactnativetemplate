import Endpoint from '../helpers/Endpoints';

export const JSONTESTDATA = '@la:JSONTESTDATA';
export const JSONTESTDATA_SUCCESS = '@la:JSONTESTDATA_SUCCESS';
export const JSONTESTDATA_ERROR = '@la:JSONTESTDATA_ERROR';

export function getUserAgency() {
    return {
        type: JSONTESTDATA,
        payload: {
            client: 'api',
            request: {
                method: 'GET',
                url: Endpoint.getJsonTestData(),
            }
        }
    };
}