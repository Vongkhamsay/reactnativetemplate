import React from 'react'
import { Router, Scene, Modal, Actions } from 'react-native-router-flux'
import { ActivityIndicator, StyleSheet } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Home from './containers/Dashboard/DashboardContainer';
import { Button, Text, View, Toast } from 'native-base';

interface ILoginProps {
}

interface ILoginState {
   hasToken: boolean
   isLoaded: boolean
}

interface IActions {

}

interface IConnectedLoginProps extends ILoginProps, IActions { }

class Routes extends React.Component<IConnectedLoginProps, ILoginState> {
   constructor(props) {
      super(props);
      this.state = {
         hasToken: false,
         isLoaded: false
      };
   }

   componentWillMount() {
      // make api call to check if authenticated
         this.setState({ isLoaded: true })
   }

   render() {
      if (!this.state.isLoaded) {
         return (

            <ActivityIndicator
               animating={!this.state.isLoaded}
               size='large'
            />

         );
      } else {
         return (
            <Router>
               <Scene key="root">
                  <Scene key="home" component={Home} />
               </Scene>
            </Router>
         )
      }
   }
}

export default Routes