import * as JsonTestDataActions from '../actions/JsonTestDataActions';
import { CreateLoaderReducer } from './GeneralReducer';
import { AsyncStorage } from 'react-native';

const initialState: any = {
	bearer: {
		inProcess: false,
		error: false,
		// CHECKS LOCALSTORAGE FOR NOW. WILL SET LOGGED IN USER TO APPLICATION STATE
		// TODO: THIS WILL NEED TO BE CHANGED WHEN WE PULL USER PROFILE FOR EACH ROUTE. SO WHEN USER STATE CHANGES IT WILL TRIGGER THE HEADER PROPS AND RERENDER THE HEADER (SO HEADER COLORS CAN CHANGE).
		data: null
	}
};

export let _childReducers = {
	loginLoaderReducer: CreateLoaderReducer({
		startAction: JsonTestDataActions.JSONTESTDATA,
		clearPayloadWith: initialState
	})
};

export default function systemHealthReducer(state: any = initialState, action: any) {
	switch (action.type) {
		case JsonTestDataActions.JSONTESTDATA:
		case JsonTestDataActions.JSONTESTDATA_SUCCESS:
		case JsonTestDataActions.JSONTESTDATA_ERROR:
			return {
				...state,
				bearer: _childReducers.loginLoaderReducer(state, action)
			};
	}
	return state;
}
